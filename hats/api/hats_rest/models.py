from django.db import models
from django.conf import settings
from django.urls import reverse


# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name

    
class Brand(models.Model):
    name = models.CharField(max_length=100)
    logo_url = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Material(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class Style(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class FamilyMember(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name
    
    
class Hat(models.Model):
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    size = models.CharField(max_length=10)
    color = models.CharField(max_length=50)
    quantity = models.IntegerField(blank=True, null=True)
    description = models.TextField()
    image_url = models.URLField(blank=True, null=True)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='hat',
        blank=True,
        null=True
    )
    brand = models.ForeignKey(
        Brand,
        on_delete=models.CASCADE,
        related_name='hat',
        blank=True,
        null=True
    )
    material = models.ForeignKey(
        Material,
        on_delete=models.CASCADE,
        related_name='hat',
        blank=True,
        null=True
    )
    style = models.ForeignKey(
        Style,
        on_delete=models.CASCADE,
        related_name='hat',
        blank=True,
        null=True
    )
    family_member = models.ForeignKey(
        FamilyMember,
        on_delete=models.CASCADE,
        related_name='hat',
        blank=True,
        null=True
    )
    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.CASCADE,
        blank=True,
        null=True
    )

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})