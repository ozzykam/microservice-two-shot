import json
import django
# import os
# import sys
# sys.path.append("")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "wardrobe_project.settings")
# django.setup()



from django.http import JsonResponse
from decimal import Decimal
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import (
    Hat, 
    LocationVO,
    Brand,
    Material,
    Style,

)


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "name",
    ]


class BrandEncoder(ModelEncoder):
    model = Brand
    properties = [
        "id",
        "name",
        "description",
        "logo_url"

    ]


class StyleEncoder(ModelEncoder):
    model = Style
    properties = [
        "name",
        "description",
    ]


class MaterialEncoder(ModelEncoder):
    model = Material
    properties = [
        "name",
        "description",
    ]


class HatListModelEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "description",
        "image_url",
        "quantity",

    ]
    def get_extra_data(self, o):
        return {"location": o.location.name}

class HatDetailModelEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "size",
        "color",
        "description",
        "image_url",
        "price",
        "quantity",
    ]
    def get_extra_data(self, o):
        foreign_keys_data = {}
        if o.location is not None:
            foreign_keys_data["location"] = o.location.name
            foreign_keys_data["href"] = o.location.import_href
        else:
            foreign_keys_data["location"] = "Unknown"
            foreign_keys_data["href"] = "Unknown"

        if o.brand is not None:
            foreign_keys_data["brand"] = o.brand.name
        else:
            foreign_keys_data["brand"] = "Unknown"
        
        if o.material is not None:
            foreign_keys_data["material"] = o.material.name
        else:
            foreign_keys_data["material"] = "Unknown"

        if o.style is not None:
            foreign_keys_data["style"] = o.style.name
        else:
            foreign_keys_data["style"] = "Unknown"
        return foreign_keys_data
    
    def default(self, obj): #this function helps to handle the price value for Json
        if isinstance(obj, Decimal):
            return float(obj)
        return super().default(obj)

# ================================================================
# HOW TO GET THE HAT DATA AND CREATE NEW HAT(S) (LINES 83-148)
# ================================================================
@require_http_methods(["GET","POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
        #--RETURNS A LIST OF HATS SPECIFIC TO A LOCATION (IF LOCATION ID IS PROVIDED)
            hats = Hat.objects.filter(location=location_vo_id)
            return JsonResponse(
                {"hats":hats},
                encoder = HatDetailModelEncoder
            )
        else:
        #--RETURNS A LIST OF ALL HATS FOR ALL LOCATIONS IF (LOCATION ID IS *NOT* PROVIDED)
            hats = Hat.objects.all()
            return JsonResponse(
                {"hat": hats},
                encoder=HatListModelEncoder,
        )
    else:
        content = json.loads(request.body)
        # --------------------------------------------
        # Error Handling for Location (LINES 100-108)
        # --------------------------------------------
        try:
            location_href = f"/api/locations/{location_vo_id}/"
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid LOCATION ID"},
                status=400
            )
    

        # --------------------------------------------
        # Error Handling for Brand (LINES 114-122)
        # --------------------------------------------
        try:
            brand_name = content.get("brand")
            brand = Brand.objects.get(id=brand_name)
            style = Style.objects.get(name=content["style"])
            content["brand"] = brand
            content["style"] = style
        except Brand.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid BRAND NAME"}, 
                status=400
            )

        # --------------------------------------------
        # Error Handling for Material (LINES 127-135)
        # --------------------------------------------
        try:
            material_name = content.get("material")
            material = Material.objects.get(name=material_name)
            content["material"] = material
        except Material.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid material name"}, 
                status=400
            )
        

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailModelEncoder,
            safe=False
            )
        

@require_http_methods(["GET","PUT","DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailModelEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, __ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid LOCATION ID"}
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailModelEncoder,
            safe=False
        )


@require_http_methods(["GET","POST"])
def api_list_brands(request):
    if request.method == "GET":
        brands = Brand.objects.all()
        return JsonResponse(
            {"brands":brands},
            encoder = BrandEncoder
            )
    else:
        content = json.loads(request.body)
        brand = Brand.objects.create(**content)

        return JsonResponse(
            brand,
            encoder=BrandEncoder,
            safe=False
        )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_brand(request, id):
    if request.method == "GET":
        brand = Brand.objects.filter(id=id)
        return JsonResponse(
            {"brand":brand},
            encoder = BrandEncoder
            )
    elif request.method == "DELETE":
        count, _ = Brand.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Brand.objects.filter(id=id).update(**content)
        brand = Brand.objects.get(id=id)
        return JsonResponse(
            brand,
            encoder=BrandEncoder,
            safe=False
        )
    
@require_http_methods(["GET","POST"])
def api_list_materials(request):
    if request.method == "GET":
        materials = Material.objects.all()
        return JsonResponse(
            {"materials":materials},
            encoder = MaterialEncoder
            )
    else:
        content = json.loads(request.body)
        material = Material.objects.create(**content)

        return JsonResponse(
            material,
            encoder=MaterialEncoder,
            safe=False
        )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_material(request, id):
    if request.method == "GET":
        material = Material.objects.filter(id=id)
        return JsonResponse(
            {"material":material},
            encoder = MaterialEncoder
            )
    elif request.method == "DELETE":
        count, _ = Material.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Material.objects.filter(id=id).update(**content)
        material = Material.objects.get(id=id)
        return JsonResponse(
            material,
            encoder=MaterialEncoder,
            safe=False
        )
    
@require_http_methods(["GET","POST"])
def api_list_styles(request):
    if request.method == "GET":
        styles = Style.objects.all()
        return JsonResponse(
            {"styles":styles},
            encoder = StyleEncoder
            )
    else:
        content = json.loads(request.body)
        style = Style.objects.create(**content)

        return JsonResponse(
            style,
            encoder=StyleEncoder,
            safe=False
        )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_style(request, id):
    if request.method == "GET":
        style = Style.objects.filter(id=id)
        return JsonResponse(
            {"style":style},
            encoder = StyleEncoder
            )
    elif request.method == "DELETE":
        count, _ = Style.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        content = json.loads(request.body)
        Style.objects.filter(id=id).update(**content)
        style = Style.objects.get(id=id)
        return JsonResponse(
            style,
            encoder=StyleEncoder,
            safe=False
        )