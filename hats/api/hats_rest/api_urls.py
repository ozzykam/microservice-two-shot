from django.urls import path
from .api_views import (
    api_list_hats,
    api_show_hat,
    api_list_brands,
    api_show_brand,
    api_list_materials,
    api_show_material,
    api_list_styles,
    api_show_style,
)


urlpatterns = [
    path("hats/", api_list_hats, name="api_list_all_hats"),
    path("locations/<int:location_vo_id>/hats/", api_list_hats, name="api_list_hats_by_location"),
    path("hat/<int:pk>/", api_show_hat, name="api_show_hat"),
    path("hats/", api_list_hats, name="api_create_hats"),
    
    path("brands/", api_list_brands, name="api_create_brands"),
    path("brand/<int:id>/", api_show_brand, name="api_show_brand"),

    path("materials/", api_list_materials, name="api_create_materials"),
    path("material/<int:id>/", api_show_material, name="api_show_material"),

    path("styles/", api_list_styles, name="api_create_styles"),
    path("style/<int:id>/", api_show_style, name="api_show_style"),

]