from django.contrib import admin
from .models import (
    Hat, 
    Brand, 
    Style, 
    FamilyMember, 
    Material, 
    LocationVO,
)
# Register your models here.

@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    location_id = LocationVO.id
    list_display = [
        "name",
        "id",
        "price",
        "location",
        "location_id",
    ]
admin.site.register(Brand)
admin.site.register(Style)
admin.site.register(FamilyMember)
admin.site.register(Material)