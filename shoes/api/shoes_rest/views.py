from django.shortcuts import render
from common.json import ModelEncoder
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
# Create your views here.



class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href", "id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
                  "manufacturer",
                  "model_name",
                  "description",
                  "color",
                  "picture_url",
                  "bin",
                  "id"
                  ]

    encoders = {
        "bin": BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "description",
        "color",
        "picture_url",
        "bin",
        "id"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):

    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoes(request, pk):

    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            response_data = {
                "manufacturer": shoe.manufacturer,
                "model_name": shoe.model_name,
                "description": shoe.description,
                "color": shoe.color,
                "picture_url": shoe.picture_url,
                "bin": shoe.bin.id
            }
            return JsonResponse(response_data)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe not found"}, status=404)
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:  # PUT method
        try:
            content = json.loads(request.body)
            if "bin" in content:
                content["bin"] = BinVO.objects.get(id=content["bin"])
            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk)
            response_data = {
                "manufacturer": shoe.manufacturer,
                "model_name": shoe.model_name,
                "description": shoe.description,
                "color": shoe.color,
                "picture_url": shoe.picture_url,
                "bin": shoe.bin.id
            }
            return JsonResponse(response_data)
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid bin ID"}, status=400)
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=500)
