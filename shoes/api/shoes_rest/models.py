from django.db import models



# Create your models here.
# class Bin(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=255, blank=True, null=True)
#     location = models.CharField(max_length=255, blank=True, null=True)
#     capacity = models.IntegerField(default=0)



class BinVO(models.Model):
    import_href = models.URLField(unique=True)
    closet_name = models.CharField(null=True, max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)

    # def get_api_url(self):
    #     return reverse("api_bin", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.closet_name} - {self.bin_number}/{self.bin_size}"

    # class Meta:
    #     ordering = ("closet_name", "bin_number", "bin_size")


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=255)
    model_name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    color = models.CharField(max_length=100)
    picture_url = models.URLField()
    bin = models.ForeignKey(BinVO, on_delete=models.CASCADE, related_name='shoes')

    def __str__(self):
        return f"{self.manufacturer} {self.model_name} - {self.color}"
