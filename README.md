# Wardrobify
Team:
* Aziz - Hats
* James - Shoes
## Design
![diagram](image-1.png)
## API Documentation
The Wardrobify project encompasses microservices for managing an extensive collection of shoes and hats, detailing CRUD operations across wardrobe setups with specific APIs for item attributes, storage solutions, and integration capabilities.
## URLs and Ports ^
Shoes API port 8080
    (http://localhost:8080/api/shoes/)
Bin API port 8100
    (http://localhost:8100/api/bins/)
Location API port 8100
    (http://localhost:8100/api/locations/)
Hats API port 8090
    (http://localhost:8090/api/hats/)
## Shoe API
Our Shoes microservice API documentation outlines how to interact with shoe and bin data, detailing endpoint paths, supported operations, data formats, and error handling for effective API integration.
## Hat API
Our Hats microservice API documentation outlines how to interact with shoe and bin data, detailing endpoint paths, supported operations, data formats, and error handling for effective API integration.
## Shoes microservice
The BinVO and Shoe models are part of a Django application that integrates with a wardrobe management microservice. BinVO stores bin details linked via URLs, for synchronization with a separate service. Shoe links to these bins, categorizing footwear by manufacturer, model, and color. This setup is used alongside Location and Bin models in the wardrobe microservice, which organizes storage by closet names, sections, and bins, facilitating structured queries and updates via RESTful endpoints with get, put, delete, and post capabilities.
## Hats microservice
The LocationVO, Hat, and other associated models are integral to a Django application that forms the Hats microservice within a comprehensive wardrobe management system. LocationVO is used to store unique location data linked via URLs for synchronization with other services. The Hat model connects to this location data, categorizing hats by name, price, size, color, and quantity, among other attributes. This setup also utilizes Brand, Material, Style, and FamilyMember models to provide detailed descriptors for hats, enriching their descriptions and storage logistics. This infrastructure supports structured data interactions through RESTful API endpoints that allow creating, reading, updating, and deleting hat information, efficiently managing the wardrobe ecosystem with detailed classification and storage solutions.
## Value Objects
    Shoes:
        Manufacturer
        Model Name
        Description
        Color
        Picture URL
        Bin
    BinVO
        Import Href
        Closet Name
        Bin Number
        Bin Size
    LocationVO
        Import Href
        Name
    Brand
        Name
        Logo URL
        Description
    Material
        Name
        Description
    Style
        Name
        Description
    Family Member
        Name
        Age
    Hat
        Name
        Price
        Size
        Color
        Quantity
        Description
        Image URL
        User
        Brand
        Material
        Style
        Family Member
        Location
Wardrobe
    Location
        Closet Name
        Section Number
        Shelf Number
    Bin
        Closet Name
        Bin Number
        Bin Size
## Google how to add tables and pictures in "Markdown file"

![]