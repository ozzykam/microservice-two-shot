document.addEventListener('DOMContentLoaded', async () => {
    const form = document.getElementById('new-bin-form');
    form.addEventListener('submit', async function (event) {
        event.preventDefault();
        const formData = {
            closet_name: document.getElementById('closet_name').value,
            bin_number: document.getElementById('bin_number').value,
            bin_size: document.getElementById('bin_size').value
        };
        try {
            const response = await fetch('http://localhost:8100/api/bins/', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(formData)
            });
            if (!response.ok) {
                throw new Error('Failed to create bin');
            }
            const result = await response.json();
            console.log('Bin created:', result);
            form.reset();
        } catch (error) {
            console.error('Error:', error.message);
        }
    });
});
