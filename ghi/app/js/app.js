function createCard(manufacturer, modelName, description, color, pictureUrl, binName) {
    return `
        <div class="card my-3 shadow-sm">
            <img src="${pictureUrl}" class="card-img-top" alt="Image of ${modelName}">
            <div class="card-body">
                <h5 class="card-title">${manufacturer} - ${modelName}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${color}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer text-muted">
                    Stored in: ${binId}
                </div>
            </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8080/api/shoes/';
    const cardRow = document.getElementById('card-row');
    let columnIndex = 0;
    const columns = [];

    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to fetch shoes. Server responded with ${response.status}: ${response.statusText}`);
        }
        const data = await response.json();
        data.shoes.forEach(async (shoe) => {
            const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
            const detailResponse = await fetch(detailUrl);
            if (!detailResponse.ok) {
                throw new Error(`Failed to fetch details for shoe ${shoe.modelName}. Server responded with ${detailResponse.status}: ${detailResponse.statusText}`);
            }
            const details = await detailResponse.json();
            const manufacturer = details.shoe.manufacturer;
            const modelName = details.shoe.modelName;
            const description = details.shoe.description;
            const color = details.shoe.color;
            const pictureUrl = details.shoe.pictureUrl;
            const binName = details.shoe.bin.id;

            const html = createCard(manufacturer, modelName, description, color, pictureUrl, binName);
            if (!columns[columnIndex]) {
                columns[columnIndex] = document.createElement('div');
                columns[columnIndex].className = 'col-md-4';
                cardRow.appendChild(columns[columnIndex]);
            }
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
        });
    } catch (e) {
        console.error(e);
        cardRow.innerHTML = `
            <div class="alert alert-danger" role="alert">
                Error: ${e.message}
            </div>
        `;
    }
});
