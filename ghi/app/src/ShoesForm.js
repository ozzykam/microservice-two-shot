import React, { useState, useEffect } from 'react';

function ShoesForm() {
  const [bins, setBins] = useState([])

  const [formData, setFormData] = useState({
    manufacturer: '',
    model_name: '',
    description: '',
    color: '',
    picture_url: '',
    bin: '',
  })

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = 'http://localhost:8080/api/shoes/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    console.log(formData)
    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        manufacturer: '',
        model_name: '',
        description: '',
        color: '',
        picture_url: '',
        bin: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;

    setFormData({
      ...formData,
      [inputName]: value
    });
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">

            <div className="form-group mb-3">
              <label htmlFor="manufacturer">Manufacturer</label>
              <input onChange={handleFormChange} required type="text" name="manufacturer" id="manufacturer" className="form-control" value={formData.manufacturer} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="model_name">Model Name</label>
              <input onChange={handleFormChange} required type="text" name="model_name" id="model_name" className="form-control" value={formData.model_name} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleFormChange} className="form-control" id="description" rows="3" name="description" value={formData.description}></textarea>
            </div>

            <div className="form-group mb-3">
              <label htmlFor="color">Color</label>
              <input onChange={handleFormChange} required type="text" name="color" id="color" className="form-control" value={formData.color} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="picture_url">Picture URL</label>
              <input onChange={handleFormChange} required type="url" name="picture_url" id="picture_url" className="form-control" value={formData.picture_url} />
            </div>

            <div className="form-group mb-3">
              <label htmlFor="bin">Bin</label>
              <select onChange={handleFormChange} required name="bin" id="bin" className="form-select" value={formData.bin}>
                <option value="">Choose a bin</option>
                {bins.map(bin => (
                  <option key={bin.id} value={bin.id}>{bin.id}</option>
                ))}
              </select>
            </div>

            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ShoesForm;
