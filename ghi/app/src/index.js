import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import './index.css';
import {BrowserRouter} from "react-router-dom"

const root = ReactDOM.createRoot(document.getElementById('root'));

async function loadData() {

  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatsResponse = await fetch('http://localhost:8090/api/hats/');
  if (!shoesResponse.ok || !hatsResponse.ok) {
    console.error(`Failed to Fetch ${shoesResponse} and/or ${hatsResponse}`)
  } else {
    const shoesData = await shoesResponse.json()
    const hatsData = await hatsResponse.json()
    root.render(
      <React.StrictMode>
        <BrowserRouter>
        <App 
          shoes={shoesData.shoes} 
          hats={hatsData.hat}
        />
        </BrowserRouter>
      </React.StrictMode>
    );
    }
  } 
loadData();
