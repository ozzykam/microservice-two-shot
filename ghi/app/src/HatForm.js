import React, {useState, useEffect} from 'react';

function HatForm() {
  const [locations, setLocations] = useState([])
  const [brands, setBrands] = useState([])
  const [materials, setMaterials] = useState([])
  const [styles, setStyles] = useState([])
  const [formData, setFormData] = useState({
    name: '',
    size: '',
    color: '',
    description: '',
    price: '',
    quantity: '',
    location: '',
    brand: '',
    material: '',
    style: '',
  })

  const getData = async () => {
    const location_url = 'http://localhost:8100/api/locations/'
    const location_response = await fetch(location_url);

    if (location_response.ok) {
      const data = await location_response.json();
      setLocations(data.locations);
    }


    const brand_url = 'http://localhost:8090/api/brands/'
    const brand_response = await fetch(brand_url);

    if (brand_response.ok) {
      const data = await brand_response.json();
      setBrands(data.brands);
    }

    const material_url = 'http://localhost:8090/api/materials/'
    const material_response = await fetch(material_url);

    if (material_response.ok) {
      const data = await material_response.json();
      setMaterials(data.materials);
    }

    const style_url = 'http://localhost:8090/api/styles/'
    const style_response = await fetch(style_url);

    if (style_response.ok) {
      const data = await style_response.json();
      setStyles(data.styles);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = `http://localhost:8090/api/locations/${formData.location}/hats/`


    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);
    
    if (response.ok) {
      setFormData({
        name: '',
        size: '',
        color: '',
        description: '',
        price: '',
        quantity: '',
        location: '',
        brand: '',
        material: '',
        style: '',
      });
    }
  }

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      //Previous form data is spread (i.e. copied) into our new state object
      ...formData,

      //On top of the that data, we add the currently engaged input key and value
      [inputName]: value 
    });
  }
  console.log(formData)
  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Hat</h1>
          <form onSubmit={handleSubmit} id="add-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.size} placeholder="Size" required type="text" name="size" id="size" className="form-control" />
              <label htmlFor="size">Size</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <label htmlFor="description">Description</label>
              <textarea onChange={handleFormChange} value={formData.description} id="description" rows="3" name="description" className="form-control"></textarea>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} value={formData.price} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}  value={formData.quantity} placeholder="Qty" required type="number" name="quantity" id="quantity" className="form-control" />
              <label htmlFor="quantity">Qty</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select">
                <option value="">Closet</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.id}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.brand} required name="brand" id="brand" className="form-select">
                <option value="">Brand</option>
                {brands.map(brand => {
                  return (
                    <option key={brand.id} value={brand.id}>{brand.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.material} required name="material" id="material" className="form-select">
                <option value="">Material</option>
                {materials.map(material => {
                  return (
                    <option key={material.id} value={material.id}>{material.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} value={formData.style} required name="style" id="style" className="form-select">
                <option value="">Style</option>
                {styles.map(style => {
                  return (
                    <option key={style.id} value={style.id}>{style.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;