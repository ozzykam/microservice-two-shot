import React, { useState, useEffect } from 'react';

function ShoesList(props) {
  const [shoes, setShoes] = useState(props.shoes);

  useEffect(() => {
    setShoes(props.shoes);
  }, [props.shoes]);
  const handleDelete = async (e, shoeId) => {
    e.preventDefault();
    const confirmDelete = window.confirm("Are you sure you want to delete this shoe?");
    if (confirmDelete) {
      const url = `http://localhost:8080/api/bin/shoes/${shoeId}`;
      try {
        const response = await fetch(url, {
          method: 'DELETE'
        });
        if (!response.ok) {
          throw new Error('Failed to delete the shoe');
        }
        setShoes(prevShoes => prevShoes.filter(shoe => shoe.id !== shoeId));
      } catch (error) {
        console.error("Delete operation failed:", error);
        alert("Failed to delete shoe");
      }
    }
  };


    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Description</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Bin</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes && props.shoes.map(shoe => (
            <tr key={`${shoe.manufacturer}-${shoe.model_name}`}>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.description}</td>
              <td>{shoe.color}</td>
              <td>
                {shoe.picture_url ? (
                  <img src={shoe.picture_url} alt={`Image of ${shoe.model_name}`} style={{ width: '100px', height: 'auto' }} />
                ) : (
                  <span>No image available</span>
                )}
              </td>
              <td>{shoe.bin.id}</td>
              <td>
                <button onClick={(event) => handleDelete(event, shoe.id)} className="btn btn-danger">Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  export default ShoesList;
