import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ShoeColumn(props) {
  return (
    <div className="col">
      {props.list.map(shoe => {
        return (
          <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture_url} className="card-img-top" alt={`Image of ${shoe.model_name}`} />
            <div className="card-body">
              <h5 className="card-title">{shoe.manufacturer} - {shoe.model_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">{shoe.color}</h6>
              <p className="card-text">{shoe.description}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
}

const MainPage = () => {
  const [shoeColumns, setShoeColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        const columns = [[], [], []];

        let i = 0;
        for (const shoe of data.shoes) {
          columns[i].push(shoe);
          i = (i + 1) % 3;
        }
        setShoeColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
        <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </p>
          <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/shoes/new" className="btn btn-primary btn-lg px-4 gap-3">Add New Shoes</Link>
          </div>
        </div>
      </div>
      <div className="container">
        <h2>Shoe Closet</h2>
        <div className="row">
          {shoeColumns.map((shoeList, index) => {
            return (
              <ShoeColumn key={index} list={shoeList} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default MainPage;
