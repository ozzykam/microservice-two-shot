import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import BinForm from './BinForm';
import HatForm from './HatForm';
import HatsList from './HatsList'

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }
  return (
<>
    {/* <BrowserRouter> */}
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path="/shoes/new" element={<ShoesForm />} />
          <Route path="/bin/new" element={<BinForm />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/hats" element={<HatsList hats={props.hats}/>} />
        </Routes>
      </div>
    {/* </BrowserRouter> */}
</>
  );
}

export default App;
