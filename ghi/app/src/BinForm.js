import React, { useEffect, useState } from 'react';

function BinForm() {
  const [formData, setFormData] = useState({
    closet_name: '',
    bin_number: '',
    bin_size: ''
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const binUrl = 'http://localhost:8100/api/bins/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(binUrl, fetchConfig);
      if (response.ok) {
        console.log('Bin created successfully:', await response.json());

        setFormData({
          closet_name: '',
          bin_number: '',
          bin_size: ''
        });
      } else {
        throw new Error('Failed to create bin');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new bin</h1>
          <form onSubmit={handleSubmit} id="create-bin-form">
            <div className="form-floating mb-3">
              <input type="text" className="form-control" id="closet_name"
                     name="closet_name" placeholder="Closet Name"
                     value={formData.closet_name} onChange={handleInputChange} required />
              <label htmlFor="closet_name">Closet Name</label>
            </div>
            <div className="form-floating mb-3">
              <input type="text" className="form-control" id="bin_number"
                     name="bin_number" placeholder="Bin Number"
                     value={formData.bin_number} onChange={handleInputChange} required />
              <label htmlFor="bin_number">Bin Number</label>
            </div>
            <div className="form-floating mb-3">
              <input type="text" className="form-control" id="bin_size"
                     name="bin_size" placeholder="Bin Size"
                     value={formData.bin_size} onChange={handleInputChange} required />
              <label htmlFor="bin_size">Bin Size</label>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default BinForm;
