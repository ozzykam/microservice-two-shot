import React, { useState, useEffect } from 'react';

function HatsList(props) {
  const [hats, setHats] = useState(props.hats || [])

  useEffect(() => {
    setHats(props.hats)
  }, [props.hats])

  const handleDelete = async (e, href) => {
    e.preventDefault()
    const confirmDelete = window.confirm("CONFIRMATION: Are you sure you want to delete?")
    if (confirmDelete) {
      const url = `http://localhost:8090${href}`
      try {
        const response = await fetch(url, { method: 'DELETE' })
        if (!response.ok) {
          throw new Error('Failed to delete selected hat')
        }
        setHats(prevHats => prevHats.filter(hat => hat.href !== href))
      } catch (error) {
        console.error("Delete operation failed:", error)
        alert("Failed to delete hat")
      }
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Image</th>
          <th>Quantity</th>
          <th>Location</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {hats && hats.map(hat => (
          <tr key={hat.href}>
            <td>{hat.name}</td>
            <td>{hat.description}</td>
            <td>
              {hat.image_url ? (
                <img src={hat.image_url} alt={`Image of ${hat.name}`} style={{ width: '100px', height: 'auto' }} />
              ) : (
                <span>No image available</span>
              )}
            </td>
            <td>{hat.quantity}</td>
            <td>{hat.location}</td>
            <td>
              <button onClick={(event) => handleDelete(event, hat.href)} className="btn btn-danger">Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default HatsList;
